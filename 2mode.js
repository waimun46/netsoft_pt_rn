
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform,
  PermissionsAndroid,
  ActivityIndicator,
  Image,
  Dimensions, Animated, Easing, PixelRatio, TouchableOpacity, TextInput
} from 'react-native';
import { Flex, NoticeBar, Icon, List, Card, Button, Slider } from '@ant-design/react-native';
import Video from 'react-native-video';
import logo from './assets/logo.jpg';
import qr from './assets/QRphoto.png';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Modal from "react-native-modal";
import RNRestart from 'react-native-restart';

import cloudys from './assets/weather_icon/cloudy.png';
import clears from './assets/weather_icon/clear.png';
import mists from './assets/weather_icon/cloudy.png';
import rains from './assets/weather_icon/rain.png';
import snows from './assets/weather_icon/snow.png';
import storms from './assets/weather_icon/storm.png';
import windys from './assets/weather_icon/clear.png';


const { width, height } = Dimensions.get('window');
const DEVICE_WIDTH = Dimensions.get('window').width;

const mode = height > width ? "portrait" : "landscape";


class Panel extends Component {
  scrollRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      size: { width, height },
      paused: true,
      selectIndex: 0,
      currentDate: '',
      currentTime: '',
      error: null,
      temperature: 0,
      weatherCondition: null,
      catcherr: null,
      data: [],
      isLoadingTem: true,
      progress: new Animated.Value(0),
      loop: true,
      uuid: '',
      mediaList: [],
      storeList: [],
      videoList: [],
      total_run: 0,
      myStatus: 1,
      deviceLoaction: [],
      marquee: [],
      barcode: [],
      visible: false,
      ipValue: '',
      ipAddress: '',
      hostId: '',
      fetchHost: '',
      weatherTime: '',
      setIntervalTime: null,
      timerSet: null,
      inRun1: 0,
      subLoading: false,
      window: mode,
      storeInterval: null,
      errorTimer: '',
      // storeHost: null,



    };
    this.updateInterval = this.updateInterval.bind(this)
    this.getHostAsyncStorage = this.getHostAsyncStorage.bind(this)

    // Dimensions handler store in state
    this.handler = dims => this.setState(dims);

    // Dimensions run addEventListener when change and restart
    Dimensions.addEventListener("change", () => {
      this.handler
      RNRestart.Restart();


    });



  }


  /************************************************ componentWillMount *************************************************/
  componentWillMount() {
    // getCurrentTime run
    setInterval(() => this.getCurrentTime(), 1000);

    // Dimensions change
    Dimensions.addEventListener("change", this.handler);

 



  }

  getHostAsyncStorage() {
    // AsyncStorage.getItem('STORE_HOST')
    // .then((valueHost) => {
    //   const data = JSON.parse(valueHost);


    //   this.getMediaList()

    // });

    // AsyncStorage.getItem("STORE_LIST_1").then(valueHost => {
    //   //console.log(mediaListStore, '----------------mediaListStore')
    //   const data = JSON.parse(valueHost);
    //   this.setState({
    //     fthhhh: data,
    //   })
    // })
  }



  /************************************************ componentWillUnmount *************************************************/
  componentWillUnmount() {
    // Important to stop updating state after unmount
    Dimensions.removeEventListener("change", this.handler);

  }

  componentDidMount() {

    this.getCurrentDate();

    this.getUUID();
    this.getIPAddress();
    // this.getHostId();
    this.barcodeFetch();
    this.nextSlide();

    /******************************** setInterval for scrollView slider *********************************/
 

    /************************************************ AsyncStorage getItem STORE_LIST *************************************************/
    // AsyncStorage.getItem("STORE_LIST").then(mediaListStore => {
    //   //console.log(mediaListStore, '----------------mediaListStore')
    //   const mediaList = JSON.parse(mediaListStore)
    //   if(mediaList.length > 0){
    //     this.setState({
    //       mediaList: mediaList,
    //       isLoading: false,
    //       selectIndex: -1,
    //     }, () => this.nextSlide())
    //   }
    // })


  }

  /************************************************ updateInterval time *************************************************/
  updateInterval() {

    let interval = setInterval(() => {
      console.log('--------------setInterval 1--------')
      this.getMediaList();
      this.barcodeFetch();
      this.updateStatus();
      this.marqueeText();
      this.setState({
        inRun1: this.state.inRun1 + 1,
        subLoading: false,
      })
    }, 1000 * Number(this.state.timerSet))
    this.setState({
      storeInterval: interval
    })


  }

  /************************************************ submitValue domain *************************************************/
  submitValue() {
    clearInterval(this.state.storeInterval);
    if (this.state.setIntervalTime === null) {
      this.setState({
        errorTimer: 'Please fill in your reflash timer',
        visible: true
      })
    } else {

      this.setState({
        fetchHost: this.state.ipValue,
        timerSet: this.state.setIntervalTime,
        errorTimer: '',
        visible: false,
        subLoading: true,
      }, () => {
        this.getMediaList(), this.marqueeText(), this.barcodeFetch(), this.updateStatus(), this.sendUUID(),
          this.updateInterval(), this.getHostAsyncStorage()
      })

    }
  }

  // checkHost(){
  //   console.log('ipValue', this.state.ipValue)
  //   if (this.state.ipValue !== '') {
  //     this.setState({
  //       visible: false
  //     })
  //   }
  // }

  /************************************************ marqueeText *************************************************/
  marqueeText(uuid = this.state.uuid, ) {
    let url = `${this.state.fetchHost}/marquee.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    console.log('marquee------');
    fetch(url).then((res) => res.json())
      .then((fetchMarquee) => {
        this.setState({
          marquee: fetchMarquee[0]
        })
      })
  }

  /************************************************ barcodeFetch *************************************************/
  barcodeFetch() {
    let url = `${this.state.fetchHost}/logo.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B`;
    // console.log('uri-----barcodeFetch', url);
    //console.log('barcodeFetch----');
    fetch(url).then((res) => res.json())
      .then((fetchBarcode) => {
        this.setState({
          barcode: fetchBarcode[0]
        })
      })
  }

  /************************************************ updateStatus *************************************************/
  updateStatus(uuid = this.state.uuid, status = this.state.myStatus) {
    let url = `${this.state.fetchHost}/device_mode.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}&dev_mode=${status}`;
    // console.log('updateStatus-------');
    fetch(url).then((res) => res.json())
      .then((statusUpdate) => {
        console.log('statusUpdate----', statusUpdate[0].status)
      })
  }

  /************************************************ getUUID *************************************************/
  getIPAddress() {
    DeviceInfo.getIpAddress().then(ip => {
      console.log('getip--------', ip)
      this.setState({
        ipAddress: ip
      }, () => {
        // this.setState({
        //   ipValue: this.state.ipAddress
        // })    
      })
    });
  }

  /************************************************ getHostId *************************************************/
  getHostId() {
    DeviceInfo.getHost().then(host => {
      console.log('host--------', host)
      this.setState({
        hostId: host
      })
    });
  }

  /************************************************ getUUID *************************************************/
  getUUID() {
    let uniqueId = DeviceInfo.getUniqueId();
    console.log('uniqueId------', uniqueId)
    AsyncStorage.setItem('STORE_UUID', uniqueId);
    this.setState({
      uuid: uniqueId,
    }, () => { this.sendUUID(), this.getMediaList(), this.updateStatus(), this.marqueeText() })
  }

  /************************************************ sendUUID *************************************************/
  sendUUID(uuid = this.state.uuid) {
    console.log('sendUUID=======================================', uuid)
    let url = `${this.state.fetchHost}/device_setup.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    fetch(url).then((res) => res.json())
      .then((sendID) => {
        if (sendID[0].status === 1) {
          // alert('Success Get Current Device UniqueId')
          console.log('Success Get Current Device UniqueId', Platform.OS === 'ios' ? `IOS: ${uuid}` : `Android: ${uuid}`)
        } else {
          alert('Failure Get Current Device UniqueId')
        }
      })
  }

  /************************************************ getMediaList *************************************************/
  getMediaList(uuid = this.state.uuid) {
    let screenType = this.state.window;
    console.log('screenType----', screenType)
    const { stayR, storeHost, fetchHost } = this.state;
    let url = `${fetchHost}/${screenType === 'landscape' ? 'media_list_landscape' : 'media_list'}.php?security_code=36BBA69CBEA56FBFE09E17B31C26D57B&uid=${uuid}`;
    console.log('=================getMediaList', url)
    console.log('--------------getMediaList ---------')
    fetch(url).then((res) => res.json())
      .then((fetchData) => {
        // console.log('fetchData', fetchData)
        // console.log('this.state.mediaList',this.state.mediaList)
        // console.log('JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList)', JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList))
        if (JSON.stringify(fetchData) != JSON.stringify(this.state.mediaList)) {
          AsyncStorage.setItem('STORE_LIST', JSON.stringify(fetchData));
          this.setState({
            mediaList: fetchData,
            deviceLoaction: fetchData[0],
            isLoading: false,
            selectIndex: -1,
          }, () => { this.nextSlide(), this.fetchWeather() })
        }
      })
  }

  /********************************************* Fetch Weather in Api *********************************************/
  fetchWeather(lat = this.state.deviceLoaction.dev_latitude, lon = this.state.deviceLoaction.dev_longitude, API_KEY = '7a522fdd67758b65b2eb6872f9f08d45') {
    //console.log(lat, lon, '---------------lat long')
    fetch(
      `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
    )
      .then((response) => {
        // console.log(response, 'response----------');
        if (!response.ok) throw new Error(response.status);
        else return response.json();
      })
      .then(json => {
        //console.log(JSON.stringify(json));
        this.setState({
          temperature: json.main.temp.toFixed(0),
          weatherCondition: json.weather[0].main,
          // weatherIcon: json.weather[0].icon,
          data: json,
          isLoadingTem: false
        })
      })
      .catch((error) => {
        //console.log(error, 'response----------error');
        this.setState({
          catcherr: error.message
        });
      });
  }

  /************************************************ getCurrentDate *************************************************/
  getCurrentDate() {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();
    let d = new Date();
    let weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    let day = weekday[d.getDay()];
    this.setState({
      currentDate: day + ' ' + date + '-' + month + '-' + year,
    });
  }

  /************************************************ getCurrentTime *************************************************/
  getCurrentTime() {
    let date, TimeType, hour, minutes, seconds, fullTime, weatherTimes;
    date = new Date();
    hour = date.getHours();
    minutes = date.getMinutes();
    seconds = date.getSeconds();
    if (hour <= 11) { TimeType = 'AM'; }
    else { TimeType = 'PM'; }
    if (hour > 12) { hour = hour - 12; }
    if (hour == 0) { hour = 12; }
    if (minutes < 10) { minutes = '0' + minutes.toString(); }
    if (seconds < 10) { seconds = '0' + seconds.toString(); }
    fullTime = hour.toString() + ':' + minutes.toString() + ':' + seconds.toString() + ' ' + TimeType.toString();
    weatherTimes = hour + TimeType;
    this.setState({
      currentTime: fullTime,
      weatherTime: weatherTimes
    });
  }

  /************************************************ setSelectIndex for scrollView slider *************************************************/
  setSelectIndex = event => {
    const viewSize = event.nativeEvent.layoutMeasurement.width;
    const contentOffset = event.nativeEvent.contentOffset.x;
    const selectIndex = Math.floor(contentOffset / viewSize);
    this.setState({
      selectIndex
    })
  }

  /************************************************ nextSlide *************************************************/
  nextSlide() {
    console.log('nextSlide run');
    // console.log(this.state.selectIndex, 'nextSlide run this.state.selectIndex');
    // console.log(this.state.mediaList, 'this.state.mediaList');
    // console.log(this.state.total_run, "total_run")

    clearTimeout(this.interval)

    // if (this.state.mediaList.length === 0 || this.state.total_run >=10) {
    if (this.state.mediaList.length === 0) {
      ;
    } else {
      console.log('nextSlide run before setState');
      this.setState({
        total_run: this.state.total_run + 1,
        selectIndex: this.state.selectIndex === this.state.mediaList.length - 1 ? 0 : this.state.selectIndex + 1,
      },
        () => {
          // console.log('nextSlide run after setState');
          // console.log(this.state.selectIndex, 'nextSlide run this.state.selectIndex');
          this.scrollRef.current.scrollTo({
            animated: true,
            y: 0,
            x: DEVICE_WIDTH * this.state.selectIndex,
          })
          if (this.state.mediaList[this.state.selectIndex].md_type === 2) {
            ;
          } else {
            this.interval = setTimeout(() => {
              // console.log(this.state.mediaList, 'this.state.mediaList');
              // console.log(this.state.selectIndex, 'this.state.selectIndex');
              // console.log(this.state.mediaList[this.state.selectIndex].md_duration, 'md_duration')
              this.nextSlide();
            }, this.state.mediaList[this.state.selectIndex].md_duration)
          }
        });
    }
  }

  /************************************************ videoEnd for video end *************************************************/
  videoEnd = (index) => {
    console.log('videoEnd=============');
    setTimeout(() => {
      this.nextSlide();
    }, 100);
    setTimeout(() => {
      this.state.videoList[index].seek(0);
    }, 500);
  }

  /****************************************** ip modal function ******************************************/
  openModal = () => {
    this.setState({ visible: true });
  };

  onClose = () => {
    this.setState({ visible: false, });
  };

  /********************************* Updata Value onChange TextInput inside modal ******************************/
  updataValue(text, field) {
    if (field == 'ipValue') { this.setState({ ipValue: text }) }
    if (field == 'setIntervalTime') { this.setState({ setIntervalTime: text }) }
  }


  render() {
    const { currentDate, temperature, weatherCondition, isLoadingTem, mediaList, isLoading, progress, loop, uuid, ipAddress, fetchHost,
      marquee, barcode, currentTime, ipValue, visible, weatherTime, setIntervalTime, size, selectIndex, videoList, deviceLoaction } = this.state;

    // AsyncStorage.setItem('STORE_HOST', JSON.stringify(this.state.fetchHost));

    console.log('uuid--------render', this.state.uuid)
    console.log(`New dimensions ${width}x${height} (${this.state.window})`);
    console.log('storeHost===========', this.state.storeHost)

    console.log('fetchHost===========', this.state.fthhhh)



    return (

      /******************************************************************* PORTRAIT ********************************************************************/
      <View style={{ position: 'relative', flex: 1 }}>

        <StatusBar hidden={true} />

        {/* <Text>{this.props.navigation.state.params.host}</Text> */}
        {
          mode === "portrait" ?
            <View style={{}}>
              <View style={{ width: '100%', }}>
                {/********************************************* slider **********************************************/}
                {
                  fetchHost === '' ? null :
                    isLoading ?
                      <View style={styles.loadingWarp}>
                        <ActivityIndicator size="smalll" color="#de2d30" />
                      </View>
                      :
                      <ScrollView
                        horizontal={true}
                        pagingEnabled
                        automaticallyAdjustContentInsets={true}
                        onMomentumScrollEnd={this.setSelectIndex}
                        ref={this.scrollRef}
                        contentContainerStyle={{ flexGrow: 1 }}
                        style={{ height: '100%' }}
                      >
                        <View style={styles.sliderWarp}>
                          {
                            mediaList.map((item, index) => {
                              return (
                                item.md_type === 2 ?
                                  <View style={[styles.container, { width: size.width, height: size.height - 50 }]}>
                                    <Video
                                      ref={(ref) => { videoList[index] = ref }}
                                      onEnd={this.videoEnd.bind(this, index)}
                                      resizeMode='contain'
                                      source={{ uri: item.md_videoPath }}
                                      style={[styles.mediaPlayer, { width: size.width, }]}
                                      volume={1}
                                      paused={selectIndex === index ? false : true}
                                    // rate={15.0}
                                    />
                                  </View> :
                                  <View style={{ width: size.width, height: size.height - 50 }}>
                                    <Image source={{ uri: item.md_imagePath }} style={styles.sliderImg} />
                                  </View>
                              )
                            })
                          }
                        </View>
                      </ScrollView>
                }

                {/************************************************ NoticeBar *************************************************/}
                <View style={[styles.noticeWrap, { position: 'absolute' }]}>
                  <NoticeBar
                    // onPress={this.openModal}
                    marqueeProps={{ loop: true, leading: 800, trailing: 800, fps: 150, style: { fontSize: RFPercentage(3), color: 'yellow', } }}
                    style={styles.noticeText}
                    // icon={false}
                    icon={null}
                  >
                    {marquee.marq_content}
                  </NoticeBar>
                </View>
              </View>


              {/********************************************* header **********************************************/}
              <View style={[styles.portrait_header, {}]}>
                <View style={{ flexDirection: 'row' }}>
                  {/********************************************* logo **********************************************/}
                  <View style={{ width: '60%', }}>
                    {
                      deviceLoaction.dev_latitude === '' || fetchHost === '' ? null :
                        <View style={{ width: '100%', paddingLeft: 10 }} >
                          <Image source={{ uri: barcode.logo }} style={styles.portrait_logo} />
                        </View>
                    }
                  </View>

                  {/********************************************* weather and time **********************************************/}
                  <TouchableOpacity style={{ width: '40%', paddingRight: 8, position: 'relative', }} onPress={this.openModal} >
                    <View style={{ width: '100%', }}>
                      {/************************************* currentTimee *********************************/}
                      <View style={{ width: '100%', paddingTop: 5, paddingBottom: 5, }}>
                        <Text style={{ textAlign: 'right', fontSize: RFPercentage(2), fontWeight: 'bold' }}>{currentTime}</Text>
                      </View>

                      {/************************************* weather icon *********************************/}
                      {
                        deviceLoaction.dev_latitude === '' || fetchHost === '' ? null :
                          isLoadingTem ?
                            <View style={[styles.loadingWarp, { marginTop: 5 }]}>
                              <ActivityIndicator size="large" color="#ccc" />
                            </View>
                            :
                            <View style={styles.portrait_weatherWrap}>
                              <View style={styles.portrait_weatherImg}>
                                <Image source={
                                  weatherCondition === 'Clear' ? clears : weatherCondition === 'Rain' ? rains : weatherCondition === 'Thunderstorm' ? storms :
                                    weatherCondition === 'Snow' ? snows : weatherCondition === 'Mist' ? mists : weatherCondition === 'Clouds' ? cloudys : windys
                                }
                                  style={styles.portrait_weatherIcon}
                                />
                              </View>
                              <View style={{ width: '30%' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                  <Text adjustsFontSizeToFit={true} style={{ fontSize: RFPercentage(4.5), textAlign: 'center', }}>{temperature}</Text>
                                  <Text style={{ fontSize: RFPercentage(3), }}>°</Text>
                                </View>
                              </View>
                            </View>
                      }
                      {/************************************* currentDate *********************************/}
                      {/* <TouchableOpacity style={{ width: '100%', paddingBottom: 5, paddingTop: 5, zIndex: 3, elevation: 30 }} onPress={this.openModal}>
                        <Text style={{ textAlign: 'right', fontSize: RFPercentage(2), fontWeight: 'bold' }} >{currentDate}</Text>
                      </TouchableOpacity> */}
                      <Text style={{ textAlign: 'right', fontSize: RFPercentage(2), fontWeight: 'bold' }} >{currentDate}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

            :


            /******************************************************************* LANDSCAPE ********************************************************************/
            <View style={{ position: 'relative', flex: 1 }}>


              <View style={{ width: '100%', }}>
                {/********************************************* slider **********************************************/}
                {
                  fetchHost === '' ? null :
                    isLoading ?
                      <View style={styles.loadingWarp}>
                        <ActivityIndicator size="smalll" color="#de2d30" />
                      </View>
                      :
                      <ScrollView
                        horizontal={true}
                        pagingEnabled
                        automaticallyAdjustContentInsets={true}
                        onMomentumScrollEnd={this.setSelectIndex}
                        ref={this.scrollRef}
                        contentContainerStyle={{ flexGrow: 1, }}
                      // style={{ height: '100%', width: this.state.size.width  }}
                      >
                        <View style={styles.sliderWarp}>
                          {
                            mediaList.map((item, index) => {
                              return (
                                item.md_type === 2 ?
                                  <View style={[styles.container, { width: size.width, height: size.height }]}>
                                    <Video
                                      ref={(ref) => {
                                        videoList[index] = ref
                                      }}
                                      onEnd={this.videoEnd.bind(this, index)}
                                      resizeMode='contain'
                                      source={{ uri: item.md_videoPath }}
                                      style={[styles.mediaPlayer, { width: size.width }]}
                                      volume={1}
                                      paused={selectIndex === index ? false : true}
                                    // rate={15.0}
                                    />
                                  </View>
                                  :
                                  <View style={{ width: size.width, height: size.height }}>
                                    <Image source={{ uri: item.md_imagePath }} style={styles.landscape_sliderImg} />
                                  </View>
                              )
                            })
                          }
                        </View>
                      </ScrollView>
                }

                {/******************************** NoticeBar *********************************/}
                <View style={[styles.noticeWrap, { position: 'absolute' }]}>
                  <NoticeBar
                    // onPress={() => alert('click')}
                    marqueeProps={{ loop: true, fps: 150, style: { fontSize: RFPercentage(2.6), color: 'yellow', } }}
                    style={styles.noticeText}
                    // icon={false}
                    icon={null}
                  >
                    {marquee.marq_content}
                  </NoticeBar>
                </View>
              </View>

              {/******************************** left content *********************************/}
              <View style={[styles.landscape_leftWrap, { height: size.height - 50 }]}>
                {/******************************** logo *********************************/}
                {
                  deviceLoaction.dev_latitude === '' || fetchHost === '' ? null :
                    <View style={{ width: '100%', height: '30%', paddingLeft: 5, paddingRight: 5 }}>
                      <Image source={{ uri: barcode.logo }} style={styles.landscape_logo} />
                    </View>
                }

                {/******************************** weather and time *********************************/}

                <View style={styles.landscape_weatherTimeWrap} >
                  <TouchableOpacity style={{ width: '100%', }} onPress={this.openModal}>
                    {/************************************* currentTime *********************************/}
                    <View style={{ width: '100%', }}>
                      <Text style={{ textAlign: 'left', fontSize: RFPercentage(1.8), fontWeight: 'bold' }}>{currentTime}</Text>
                      {/* <Text style={{ textAlign: 'left', fontSize: RFPercentage(1.8), fontWeight: 'bold' }}>{currentDate}</Text> */}
                    </View>
                    {/************************************* weather icon *********************************/}
                    {
                      deviceLoaction.dev_latitude === '' || fetchHost === '' ? null :
                        isLoadingTem ?
                          <View style={[styles.loadingWarp, { marginTop: 5 }]}>
                            <ActivityIndicator size="large" color="#ccc" />
                          </View>
                          :
                          <View style={{ width: '100%', bottom: 0, }}>
                            <View style={[styles.portrait_weatherWrap,]}>
                              <View style={styles.landscape_weather}>
                                <Image source={
                                  weatherCondition === 'Clear' ? clears : weatherCondition === 'Rain' ? rains : weatherCondition === 'Thunderstorm' ? storms :
                                    weatherCondition === 'Snow' ? snows : weatherCondition === 'Mist' ? mists : weatherCondition === 'Clouds' ? cloudys : windys
                                }
                                  style={styles.landscape_weatherImg}
                                />
                              </View>
                            </View>

                            <View style={{ width: '100%', marginTop: -5 }}>
                              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Text adjustsFontSizeToFit={true} style={{ fontSize: RFPercentage(4), textAlign: 'center', }}>
                                  {temperature}
                                </Text>
                                <Text style={{ fontSize: RFPercentage(3), }} >°</Text>
                              </View>
                            </View>
                          </View>
                    }
                    {/************************************* currentDate *********************************/}
                    {/* <TouchableOpacity style={{ width: '100%', paddingBottom: 5, paddingTop: 5, }} onPress={this.openModal}>
                      <Text style={{ textAlign: 'left', fontSize: RFPercentage(1.8), fontWeight: 'bold' }} >{currentDate}</Text>
                    </TouchableOpacity> */}
                    <Text style={{ textAlign: 'left', fontSize: RFPercentage(1.8), fontWeight: 'bold' }} >{currentDate}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

        }


        {/************************************************ Modal key in domain or ip address *************************************************/}
        <Modal isVisible={visible}
          // onBackdropPress={() => this.setState({ visible: false })}
          // swipeDirection={['up', 'left', 'right', 'down']}
          style={{ margin: 'auto', alignItems: 'center' }}
        >
          <View style={[styles.modalWarp, { width: mode === 'landscape' ? '60%' : '90%', }]}>
            <View style={{ alignItems: 'center', padding: 22, }}>
              <View style={{ alignItems: 'center', }}>
                <Text style={{ marginBottom: 5, fontWeight: 'bold', fontSize: RFPercentage(3), }}>Please setup your device</Text>
                <Text style={{ marginBottom: 5, textAlign: 'center' }}>please key in your domain or current IP address.</Text>
                <Text style={{ marginBottom: 20, color: '#d1383d' }}>Your Current IP Address is : {ipAddress}</Text>
              </View>
              <TextInput
                value={ipValue}
                placeholder="Fill in your Domain or IP Address"
                placeholderTextColor='#ccc'
                style={[styles.inputstyle, { marginBottom: 10 }]}
                onChangeText={(text) => this.updataValue(text, 'ipValue')}
                autoCorrect={false}
                autoCapitalize="none"
              />
              <TextInput
                value={setIntervalTime}
                placeholder="Please set your reflash timer (exp : 3 = 3s)"
                placeholderTextColor='#ccc'
                style={styles.inputstyle}
                onChangeText={(text) => this.updataValue(text, 'setIntervalTime')}
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="default"
              />
              <Text style={{ color: 'red', textAlign: 'left', paddingTop: 5, alignItems: 'flex-start' }}>{this.state.errorTimer}</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Button onPress={this.onClose} style={styles.modalBtn}>
                <Text style={{ color: '#9199a1' }}>Close</Text>
              </Button>
              <Button onPress={() => this.submitValue()} style={[styles.modalBtn, { borderLeftWidth: 0, }]}>
                <Text style={{ color: '#0077cc' }}>Confirm</Text>
              </Button>
            </View>
          </View>
        </Modal>



      </View>





    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, },
  mediaPlayer: { position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, backgroundColor: 'black', },
  bannerwarp: { paddingVertical: 3, position: 'relative' },
  bannertextwarp: { position: 'absolute', top: 10, flexDirection: 'row', padding: 10, },
  bannerSty: { width: '100%', height: '100%', resizeMode: 'contain', },
  contentWarp: { width: '100%', backgroundColor: '#fff', borderRadius: 10, padding: 30, width: '100%' },
  view: { justifyContent: 'flex-end', margin: 0, },
  inputstyle: { color: '#000', paddingLeft: 20, paddingRight: 20, width: '100%', height: 50, borderWidth: .5, borderColor: '#ccc' },
  modalBtn: { width: '50%', borderRadius: 0, borderColor: '#eee' },
  modalBackground: { flex: 1, alignItems: 'center', flexDirection: 'column', justifyContent: 'space-around', backgroundColor: '#00000040' },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF', height: 100, width: 100, borderRadius: 10, display: 'flex',
    alignItems: 'center', justifyContent: 'space-around'
  },

  sliderWarp: { flex: 1, flexDirection: 'row', flexWrap: 'wrap', position: 'relative', },
  loadingWarp: { flex: 1, justifyContent: 'center', alignItems: 'center', },
  sliderImg: { width: '100%', height: '100%', resizeMode: 'stretch', },
  noticeWrap: { width: '100%', height: 50, height: height, justifyContent: 'flex-end' },
  noticeText: { height: 50, backgroundColor: '#000', color: 'yellow', },
  modalWarp: { backgroundColor: 'white', justifyContent: 'center', borderColor: 'rgba(0, 0, 0, 0.1)', },


  portrait_header: { width: '100%', position: 'absolute', zIndex: 1, elevation: 1, backgroundColor: '#dcdcdc85' },
  portrait_logo: { width: '45%', height: '100%', resizeMode: 'contain' },
  portrait_weatherImg: { justifyContent: 'flex-end', width: '60%', alignItems: 'flex-end', height: 50, paddingRight: 5, marginTop: -5 },
  portrait_weatherIcon: { width: '80%', height: '100%', textAlign: 'center', resizeMode: 'contain', },
  portrait_weatherWrap: { flexDirection: 'row', justifyContent: 'flex-end', width: '100%', },

  landscape_leftWrap: { width: '15%', backgroundColor: '#dcdcdc85', position: 'absolute', zIndex: 1, elevation: 1, },
  landscape_weatherTimeWrap: {
    width: '100%', height: '40%', justifyContent: 'flex-end', alignItems: 'flex-end', bottom: 20, position: 'absolute',
    paddingRight: 5, paddingLeft: 5
  },
  landscape_weather: { justifyContent: 'center', width: '100%', alignItems: 'center', height: 80, },
  landscape_weatherImg: { width: '100%', height: '100%', textAlign: 'center', resizeMode: 'contain', },
  landscape_sliderImg: { width: '100%', height: '100%', resizeMode: 'stretch', },
  landscape_logo: { width: '100%', height: '100%', resizeMode: 'contain' }



})
export default Panel;



